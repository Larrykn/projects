import React from "react";
import MainLayout from "../layout/MainLayout";
import AuthPage from "../pages/auth";
import { useSelector } from "react-redux";

const Dashboard = () => {
  const { isAuthenticated } = useSelector((state) => state.auth);
  console.log(isAuthenticated);
  return <>{!isAuthenticated ? <AuthPage /> : <MainLayout />}</>;
};

export default Dashboard;

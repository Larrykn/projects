import React, { useEffect, useReducer } from "react";
import { Wrapper, Logo } from "../../../atoms";
import { Card, Row, Col } from "antd";
import { Footer } from "../../../molecules";
import { useSelector } from "react-redux";
import pathnames from "../../../../config/routes";
import { Login, Registration, PasswordRecovery } from "./Forms";

const AuthPage = () => {
  const { authPage } = useSelector((state) => state.setting);
  console.log(authPage.path);
  const renderAuthPage = () => {
    switch (authPage.path) {
      case "login":
        return <Login />;
      case "register":
        return <Registration />;
      case "recover-password":
        return <PasswordRecovery />;
      default:
        return <Login />;
    }
  };
  return (
    <>
      <Wrapper
        display="flex"
        justify="center"
        align="center"
        className="min-vh-100 overflow-auto"
        style={{ backgroundColor: "var(--clr-main)", padding: "10px 0 100px" }}
      >
        <Row className="site-authenication-layout justify-content-center">
          <Col xs={22} sm={22} md={14} lg={14} xl={14} xxl={18}>
            <Logo width={250} />
            <div className="mb-3 text-center ">
              <h4 className="text-uppercase text-muted fw-bold">
                welcome to students online voting system
              </h4>
              <p className="fw-bold my-3 text-muted">
                {!authPage.path && pathnames.login?.message}
                {authPage.path === "recover-password"
                  ? pathnames.recoverPassword?.message
                  : pathnames[authPage.path]?.message}
              </p>
            </div>
            <Card
              className="bg-white border"
              style={{
                margin: "0 auto",
                backgroundColor: "transparent",
              }}
            >
              {renderAuthPage()}
            </Card>
          </Col>
          <Footer positioned showContact />
        </Row>
      </Wrapper>
    </>
  );
};

export default AuthPage;

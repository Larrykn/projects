import React from "react";
import { Wrapper, TextInput, ButtonItem } from "../../../../atoms";
import { resgistration } from "../../../../data/forms/authforms";
import { authActions, settingActions } from "../../../../../config/actions";
import { useDispatch } from "react-redux";
import { Form } from "antd";
import pathnames from "../../../../../config/routes";

const Registration = () => {
  const dispatch = useDispatch();
  const handleFinish = (values) => {
    dispatch(authActions.registerApplicant({ payload: values }));
  };

  return (
    <>
      <Form layout="vertical" requiredMark={false} onFinish={handleFinish}>
        {resgistration.map((field, index) => (
          <TextInput
            key={`registrationfield-${index + 1}`}
            name={field.name}
            htmlFor={field.htmlFor}
            label={<span className="text-muted">{field.label}</span>}
            tooltip={field.tooltip}
            rules={field.rules}
            prefix={field.prefix}
            placeholder={field.placeholder}
          />
        ))}

        <ButtonItem htmlType="submit" text="Register" type="primary" block />
        <Wrapper
          className="form-sections-buttons mt-2"
          display="flex"
          justify="center"
        >
          <span
            style={{ color: "#1890ff", cursor: "pointer" }}
            role="link"
            onClick={() =>
              dispatch(settingActions.switchAuthPage(pathnames.login))
            }
          >
            I have an account
          </span>
        </Wrapper>
      </Form>
    </>
  );
};

export default Registration;

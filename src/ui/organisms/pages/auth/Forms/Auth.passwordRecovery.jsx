import React from "react";
import { Wrapper, TextInput, ButtonItem } from "../../../../atoms";
import { passwordrecovery } from "../../../../data/forms/authforms";
import { useDispatch } from "react-redux";
import { settingActions } from "../../../../../config/actions";
import pathnames from "../../../../../config/routes";
import { Form } from "antd";

const PasswordRecovery = () => {
  const dispatch = useDispatch();
  const handleFinish = (values) => {
    console.log(values);
  };
  return (
    <>
      <Form
        layout="vertical"
        requiredMark={false}
        onFinish={(values) => handleFinish(values)}
      >
        {passwordrecovery.map((field, index) => (
          <TextInput
            key={`passwordrecoveryfield-${index + 1}`}
            name={field.name}
            htmlFor={field.htmlFor}
            label={<span className="text-muted">{field.label}</span>}
            tooltip={field.tooltip}
            rules={field.rules}
            prefix={field.prefix}
            placeholder={field.placeholder}
          />
        ))}
        <ButtonItem htmlType="submit" text="Register" type="primary" block />
        <Wrapper
          className="form-sections-buttons mt-2"
          display="flex"
          justify="center"
        >
          <span
            style={{ color: "#1890ff", cursor: "pointer" }}
            role="link"
            onClick={() =>
              dispatch(settingActions.switchAuthPage(pathnames.login))
            }
          >
            Back to login
          </span>
        </Wrapper>
      </Form>
    </>
  );
};

export default PasswordRecovery;

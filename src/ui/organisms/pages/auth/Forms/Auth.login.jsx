import React from "react";
import { Wrapper, TextInput, ButtonItem } from "../../../../atoms";
import { Form } from "antd";
import { login } from "../../../../data/forms/authforms";
import { useDispatch, useSelector } from "react-redux";
import { authActions, settingActions } from "../../../../../config/actions";
import pathnames from "../../../../../config/routes";
import { FaSignInAlt } from "react-icons/fa";

const Login = () => {
  const dispatch = useDispatch();
  const { loginError, isAuthenticated } = useSelector((state) => state.auth);
  const handleFinish = (values) => {
    dispatch(authActions.getAuthUserRequest({ payload: values }));
    console.log(dispatch(authActions.getAuthUserRequest(values)));
    console.log("user logged in");
  };

  return (
    <>
      <Form layout="vertical" requiredMark={false} onFinish={handleFinish}>
        {login.map((field, index) => (
          <TextInput
            key={`loginfield-${index + 1}`}
            name={field.name}
            htmlFor={field.htmlFor}
            label={<span className="text-muted">{field.label}</span>}
            tooltip={field.tooltip}
            rules={field.rules}
            prefix={field.prefix}
            placeholder={field.placeholder}
          />
        ))}

        <ButtonItem
          htmlType="submit"
          text="Login"
          type="primary"
          block
          icon={<FaSignInAlt />}
        />
        <Wrapper
          className="form-sections-buttons mt-2"
          display="flex"
          justify="space-between"
        >
          <span
            style={{ color: "#1890ff", cursor: "pointer" }}
            role="link"
            onClick={() =>
              dispatch(settingActions.switchAuthPage(pathnames.register))
            }
          >
            I don't have an account
          </span>
          <span
            style={{ color: "#1890ff", cursor: "pointer" }}
            role="link"
            onClick={() =>
              dispatch(settingActions.switchAuthPage(pathnames.recoverPassword))
            }
          >
            Forgot password
          </span>
        </Wrapper>
      </Form>
    </>
  );
};

export default Login;

import React from "react";
import { Card, Row, Col, Typography, Divider, Pagination } from "antd";
import { DataSpinner, DocumentHeader, Wrapper } from "../../../../atoms";
import { useState } from "react";

const GuidePage = ({ setContent }) => {
  const [current, setCurrent] = useState(1);
  const handleChange = (key) => {
    console.log(key);
    setCurrent(key);
    setContent((prevState) => {
      return {
        ...prevState,
        subPath: `page ${key}`,
      };
    });
  };
  // const renderPage = () => {
  //   switch (current) {
  //     case 1:
  //       return <h1>Page 1</h1>;
  //     case 2:
  //       return <h1>Page 2</h1>;
  //     case 3:
  //       return <h1>Page 3</h1>;
  //     case 4:
  //       return <h1>Page 4</h1>;
  //     default:
  //       return <h1>Page 1</h1>;
  //   }
  // };
  return (
    <Card className="bg-white" style={{ backgroundColor: "transparent" }}>
      <Row className="text-center " justify="center">
        <Col xs={22} xl={18}>
          <DocumentHeader textFor="guidelines" />
          <Typography.Title level={5} className="text-uppercase ">
            Please read Through these Guidelines before attempting to apply or
            cast your vote
            <div className="text-danger">
              (Note: This form can not be edited after submiting)
            </div>
          </Typography.Title>
        </Col>
      </Row>
      <Divider></Divider>
      <Card className="w-100 mb-2">
        <Wrapper display="flex" justify="center">
          <DataSpinner width={40} height={40} />
        </Wrapper>
      </Card>
      <Divider></Divider>
      <Wrapper display="flex" justify="center">
        <Pagination
          showTitle
          responsive
          current={current}
          defaultCurrent={1}
          total={40}
          onChange={handleChange}
        />
      </Wrapper>
    </Card>
  );
};

export default GuidePage;

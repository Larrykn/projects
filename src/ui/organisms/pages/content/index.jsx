export { default as FormPage } from "./formpage/index";
export { default as Profilepage } from "./Profilepage";
export { default as HomePage } from "./homepage";
export { default as VotingPage } from "./votingpage";
export { default as GuidePage } from "./guidelinepage";
export { default as NotificationPage } from "./notifications";

import React, { useState } from "react";
import { Row, Col, Card, Tabs, Image } from "antd";
import AccountDetails from "./AccountDetails";
import ChangePassword from "./ChangePassword";
import { AvatarItem, ButtonItem, Logo, Wrapper } from "../../../../atoms";

const Profilepage = ({ content, setContent }) => {
  const handleChange = (key) => {
    console.log(key);
    setContent((prevState) => {
      return {
        ...prevState,
        subPath: key || null,
      };
    });
  };

  const handleClick = () => {
    console.log("preview user image");
  };
  return (
    <>
      <Wrapper className="bg-white">
        <Wrapper
          height={200}
          style={{ backgroundColor: "#002140" }}
          className="pt-5"
        >
          {/* <Logo width={150} color /> */}
        </Wrapper>
        <Row
          gutter={[20, 10]}
          style={{ marginTop: -60, zIndex: 100 }}
          className=" px-2 px-md-4 py-4  "
        >
          <Col xs={24} xl={6}>
            <Card>
              <Wrapper className="user-profile-avatar">
                <Row gutter={[0, 20]}>
                  <Col span={24} className="d-flex justify-content-center">
                    <AvatarItem size={150} />
                  </Col>
                  <Col span={24}>
                    <ButtonItem
                      hasEvent
                      text="View profile picture"
                      type="primary"
                      handleClick={handleClick}
                      block
                    />
                  </Col>
                </Row>
              </Wrapper>
            </Card>
          </Col>
          <Col xs={24} xl={18}>
            <Card>
              <Tabs
                defaultActiveKey="account details"
                onChange={handleChange}
                items={[
                  {
                    label: (
                      <span className="fs-6 text-capitalize">
                        account details
                      </span>
                    ),
                    key: "account details",
                    children: <AccountDetails />,
                  },
                  {
                    label: (
                      <span className="fs-6 text-capitalize">
                        change password
                      </span>
                    ),
                    key: "change password",
                    children: <ChangePassword />,
                  },
                ]}
              />
            </Card>
          </Col>
        </Row>
      </Wrapper>
    </>
  );
};

export default Profilepage;

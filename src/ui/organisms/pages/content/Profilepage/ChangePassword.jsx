import React from "react";
import { Row, Col, Form, Input, Card, Divider } from "antd";
import { Wrapper, TextInput, ButtonItem } from "../../../../atoms";
import { changePassword } from "../../../../data/forms/changepassword/changepassword";
import useFormErrors from "../../../../../config/hooks/useFormErrors";

const ChangePassword = () => {
  const { hasErrors, onFieldsChange } = useFormErrors();
  return (
    <>
      <Form layout="vertical" onFieldsChange={onFieldsChange}>
        <Row className="change-user-password-container">
          <Col xs={24} xl={12} className="">
            {changePassword.map((input, i) => (
              <TextInput
                key={i}
                {...input}
                rules={input.rules}
                dependencies={input.dependencies}
              />
            ))}
          </Col>
        </Row>

        <Divider></Divider>
        <Wrapper className="ps-3">
          <ButtonItem text="submit" htmlType="submit" disabled={hasErrors} />
        </Wrapper>
      </Form>
    </>
  );
};

export default ChangePassword;

import React from "react";
import { Row, Col, Card, Form, Divider } from "antd";
import { ButtonItem, SelectItem, TextInput, Wrapper } from "../../../../atoms";

const AccountDetails = () => {
  return (
    <>
      <Form
        layout="vertical"
        initialValues={{
          firstname: "LARRY",
          othernames: "EKINI",
          tel: "+256 777051249",
          gender: "Male",
          email: "larrykn06@gmail.com",
        }}
      >
        <Row gutter={[20, 0]} className="py-2 ">
          <Col xs={24} xl={12}>
            <TextInput
              name="firstname"
              label="first name"
              disabled
              rules={[
                {
                  required: true,
                  message: "please enter your first name",
                },
              ]}
            />
          </Col>
          <Col xs={24} xl={12}>
            <TextInput
              name="othernames"
              label="other names"
              disabled
              rules={[
                {
                  required: true,
                  message: "please enter your other name",
                },
              ]}
            />
          </Col>
          <Col xs={24} xl={12}>
            <TextInput
              name="email"
              label="email"
              rules={[
                {
                  required: true,
                  message: "please enter your email",
                },
              ]}
            />
          </Col>
          <Col xs={24} xl={12}>
            <TextInput
              name="tel"
              label="telephone number"
              rules={[
                {
                  required: true,
                  message: "please enter your telephone number",
                },
              ]}
            />
          </Col>

          <Col xs={24} xl={12}>
            <SelectItem
              name="gender"
              label="gender"
              allowClear
              rules={[
                {
                  required: true,
                  message: "please enter your gender",
                },
              ]}
              options={["male", "female"]}
            />
          </Col>
        </Row>
        <Wrapper>
          <Divider></Divider>
          <ButtonItem text="update" htmlType="submit" />
        </Wrapper>
      </Form>
    </>
  );
};

export default AccountDetails;

import React from "react";
import { Card, Row, Col, Carousel } from "antd";
import advert1 from "../../../../../assets/temp/userAd-1.png";
import advert2 from "../../../../../assets/temp/userAd-2.png";
import advert3 from "../../../../../assets/temp/userAd-3.png";
import advert4 from "../../../../../assets/temp/userAd-4.png";
import { Wrapper } from "../../../../atoms";

const HomePage = () => {
  return (
    <Wrapper>
      <Row>
        <Col span={24}>
          <Carousel autoplay>
            <div className="d-flex justify-content-center">
              <img src={advert1} width="100%" height={300} />
            </div>
            <div className="d-flex justify-content-center">
              <img src={advert2} width="100%" height={300} />
            </div>
            <div className="d-flex justify-content-center">
              <img src={advert3} width="100%" height={300} />
            </div>
            <div className="d-flex justify-content-center">
              <img src={advert4} width="100%" height={300} />
            </div>
          </Carousel>
        </Col>
      </Row>
      <Card className="mt-2">
        <Wrapper className="mb-3">
          <div>
            <h1>Latest News</h1>
          </div>
          <Row gutter={[20, 10]}>
            <Col xs={24} xl={6}>
              <Card className="bg-info">
                <h3>News content 4</h3>
              </Card>
            </Col>
            <Col xs={24} xl={6}>
              <Card className="bg-secondary">
                <h3>News content 4</h3>
              </Card>
            </Col>
            <Col xs={24} xl={6}>
              <Card className="bg-primary">
                <h3>News content 4</h3>
              </Card>
            </Col>
            <Col xs={24} xl={6}>
              <Card className="bg-success">
                <h3>News content 4</h3>
              </Card>
            </Col>
          </Row>
        </Wrapper>
        <Wrapper>
          <div>
            <h1>Vote now</h1>
          </div>
          <Row></Row>
        </Wrapper>
      </Card>
    </Wrapper>
  );
};

export default HomePage;

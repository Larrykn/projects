import React, { useState } from "react";
import {
  Card,
  Typography,
  Tooltip,
  List,
  Checkbox,
  Divider,
  Row,
  Col,
} from "antd";
import { AvatarItem, CardItem, Wrapper } from "../../../../atoms";
import { SingleNotification, NotificationList } from "../../../../molecules";
import systemIcon from "../../../../../assets/images/logo-mobile.svg";
import data from "../../../../data/notifications/data";
import { nanoid } from "nanoid";
import { DeleteOutlined } from "@ant-design/icons";
import { truncate } from "lodash";
import { MdArrowBack } from "react-icons/md";
import { IoReturnUpBack } from "react-icons/io5";
import { useEffect } from "react";

const NotificationPage = () => {
  const [collapse, setCollapse] = useState(false);
  const [items, setItems] = useState(data);
  const [message, setMessage] = useState({
    data: null,
    type: "all",
    item: items,
  });

  const renderNotification = (e) => {
    let id = e?.currentTarget?.id;

    const target = items.find((item) => item.key === id);
    if (id) {
      setMessage((prevState) => {
        return {
          ...prevState,
          itemId: id,
          type: id === "back" ? "all" : "single",
          item: id === "back" ? items : target,
        };
      });
    } else {
      id = "back";
    }
    console.log(message.type);
    switch (message.type) {
      case "single":
        return <SingleNotification message={message.item} renderItem={renderNotification} />;
      case "all":
        return (
          <NotificationList
            list={message.item}
            renderItem={renderNotification}
          />
        );
      default:
        return <h1>Error!</h1>;
    }
  };

  return (
    <Card className="bg-white" style={{ backgroundColor: "transparent" }}>
      <div className="my-3">
        <div className="p-2 border my-3" style={{ backgroundColor: "#f5f5f5" }}>
          <span
            className="fs-6 text-uppercase fw-bold text-muted ps-4"
            role="label"
          >
            unread messages [ {data.length} ]
          </span>
        </div>
        <CardItem>
          {renderNotification()}
        </CardItem>
      </div>
    </Card>
  );
};

export default NotificationPage;

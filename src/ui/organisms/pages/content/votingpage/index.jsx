import React from "react";
import { DocumentHeader, Wrapper } from "../../../../atoms";
import { FaVoteYea } from "react-icons/fa";
import { Card, Row, Col, Typography } from "antd";
import VotingList from "./VotingList";
import { subPathnames } from "../../../../../config/routes";

const VotingPage = ({ content, setContent }) => {
  const handleClick = (e) => {
    e.preventDefault();
    setContent((prevState) => {
      return {
        ...prevState,
        mainPath: subPathnames.guide.mainPath,
        subPath: subPathnames.guide.subPath,
        tailPath: null,
      };
    });
  };

  return (
    <Card className="bg-white" style={{ backgroundColor: "transparent" }}>
      <Row className="text-center" justify="center">
        <Col xs={22} xl={18}>
          <DocumentHeader textFor="voting page" />
          <Typography.Title level={5} className="text-uppercase ">
            Please we recommend you to read Through the Guide and accurately
            fill in the balot paper
            <div className="text-danger">
              (Note: voting can only be done once and one individual per
              category)
            </div>
          </Typography.Title>
        </Col>
      </Row>

      <Card
        title={
          <Wrapper className="text-capitalize d-flex" gap={12} align="center">
            <FaVoteYea size={40} />
            <span className="fs-6">students e-balot 2022</span>
          </Wrapper>
        }
        extra={
          <a
            href="guide"
            className="fs-6 text-capitalize fw-bold"
            onClick={handleClick}
          >
            <span>read guidelines</span>
          </a>
        }
        bodyStyle={{ padding: "20px 0" }}
        bordered={false}
      >
        <VotingList content={content} setContent={setContent} />
      </Card>
    </Card>
  );
};

export default VotingPage;

import React from "react";
import { Collapse, Row, Form, Divider } from "antd";
import { votingList } from "../../../../data/votinglist/testing";
import VoteCard from "../../../../molecules/votecard";
import { ButtonItem, NoData, RadioInput, Wrapper } from "../../../../atoms";
import { useState } from "react";

const { Panel } = Collapse;

const VotingList = ({ content, setContent }) => {
  const [selection, setSelection] = useState("no selection");

  const onChange = (key) => {
    setContent((prevState) => {
      return {
        ...prevState,
        subPath: key || null,
      };
    });
  };
  const handleChange = (e) => {
    setSelection(e.target.value);
  };

  const handleFinish = (values) => {
    const userVote = { user: "userTest", results: { ...values } };
    console.log(userVote);
  };

  if (votingList.data?.length === 0)
    return (
      <NoData
        height={300}
        description={
          <Typography.Paragraph className="text-uppercase fw-bold text-danger">
            sorry.. no applicants in this category
          </Typography.Paragraph>
        }
        width="100%"
      />
    );
  return (
    <>
      <Form
        layout="vertical"
        onFinish={handleFinish}
        className="vote-casting-form"
      >
        <Collapse
          className="students-votng-list"
          defaultActiveKey={["1"]}
          onChange={onChange}
          accordion
        >
          {votingList.data?.map((item, index) => (
            <Panel
              key={item.label}
              header={
                <span
                  className="fs-6 text-uppercase fw-bold text-muted"
                  role="label"
                >
                  {item.label}
                </span>
              }
              extra={
                <span className="fs-6 text-uppercase fw-bold text-muted">
                  {`[${item.data?.length}]`}
                </span>
              }
            >
              <Form.Item
                name={item.name}
                label={<span className="visually-hidden">{item.name}</span>}
              >
                <RadioInput
                  onChange={handleChange}
                  style={{ width: "100%" }}
                  defaultValue={selection}
                >
                  <Row gutter={[15, 10]}>
                    <VoteCard data={item.data} />
                  </Row>
                </RadioInput>
              </Form.Item>
            </Panel>
          ))}
        </Collapse>
        <Divider></Divider>
        <Wrapper className="px-3">
          <ButtonItem text="submit" htmlType="submit" />
        </Wrapper>
      </Form>
    </>
  );
};

export default VotingList;

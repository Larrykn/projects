import React, { useEffect, useState } from "react";
import { Row, Form, Card, Divider, Steps } from "antd";
import { Wrapper, ButtonItem, DataSpinner } from "../../../../atoms";
import formInputSelector from "../../../../utils/formInputSelector";
import {
  personaldetails,
  contactdetails,
} from "../../../../data/forms/applicationform";

const { Step } = Steps;

const ApplicationForm = ({ content, setContent, activeTab, setActiveTab }) => {
  const [current, setCurrent] = useState(0);

  const [formItems, setFormItems] = useState({
    isLoading: true,
    endpoint: [],
  });

  useEffect(() => {
    setActiveTab(formItems.endpoint[current]?.title);
  }, [activeTab]);

  useEffect(() => {
    const renderInputs = async () => {
      try {
        const data = await Promise.all([
          formInputSelector(personaldetails),
          formInputSelector(contactdetails),
        ]);
        if (!data) throw Error("failed to load items");
        setFormItems((prevState) => ({
          ...prevState,
          endpoint: [
            { title: "personal details", content: data[0] },
            { title: "contact details", content: data[1] },
            { title: "other details", content: <h1>To be fetched</h1> },
          ],
        }));
      } catch (err) {
        console.log(err.message);
      } finally {
        setFormItems((prevState) => ({ ...prevState, isLoading: false }));
      }
    };

    renderInputs();
  }, []);

  const getLocation = (operation) => {
    const sign = operation === "add" ? +1 : -1;
    setContent((prevState) => {
      let location;
      formItems.endpoint.forEach((item) => {
        if (item.title === formItems.endpoint[current + sign]?.title) {
          location = item.title;
        }
      });
      return {
        ...prevState,
        tailPath: location || null,
      };
    });
  };

  const handleFinish = (values) => {
    setCurrent(current + 1);
    getLocation("add");
    console.log(values);
  };

  const previous = () => {
    setCurrent(current - 1);
    getLocation("subtract");
  };

  return (
    <>
      <Card
        className="w-100 mb-2 my-3"
        title={
          <Steps current={current} responsive>
            {formItems.endpoint?.map((item) => (
              <Step
                key={item.title}
                title={<span className="text-capitalize">{item.title}</span>}
              />
            ))}
          </Steps>
        }
      >
        <Form
          layout="vertical"
          onFinish={handleFinish}
          initialValues={{ prefix: "+256" }}
        >
          <Row gutter={[20, 0]}>
            {formItems.isLoading ? (
              <Wrapper
                display="flex"
                justify="center"
                width="100%"
                className="py-3"
              >
                <DataSpinner width={40} height={40} />
              </Wrapper>
            ) : (
              formItems.endpoint[current]?.content
            )}
          </Row>
          <Divider className="my-1"></Divider>
          <Wrapper
            display="flex"
            justify="space-between"
            gap={10}
            className="pt-3"
          >
            <div className="form-step-controls d-flex gap-2">
              {current > 0 && (
                <ButtonItem text="Prev" hasEvent handleClick={previous} />
              )}
              {current <= formItems.endpoint.length - 1 && (
                <Form.Item>
                  <ButtonItem
                    text="Submit"
                    htmlType="submit"
                    handleClick={handleFinish}
                  />
                </Form.Item>
              )}
            </div>
          </Wrapper>
        </Form>
      </Card>
    </>
  );
};

export default ApplicationForm;

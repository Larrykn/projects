import React, { useState, useRef } from "react";
import { Row, Col, Card, Typography, Tabs } from "antd";
import { DocumentHeader } from "../../../../atoms";
import ApplicationForm from "./ApplicationForm";
import NominationForm from "./NominationForm";
import { subPathnames } from "../../../../../config/routes";

const FormPage = ({ content, setContent }) => {
  const [activeTab, setActiveTab] = useState(subPathnames.forms.tailPath);

  const handleChange = (key) => {
    setActiveTab(key);
    setContent((prevState) => {
      return {
        ...prevState,
        subPath: key,
        tailPath: key !== "nomination form" ? activeTab : null,
      };
    });
  };

  return (
    <>
      <Card className="bg-white" style={{ backgroundColor: "transparent" }}>
        <Row className="text-center " justify="center">
          <Col xs={22} xl={18}>
            <DocumentHeader textFor="form page" />
            <Typography.Title level={5} className="text-uppercase ">
              Please we recommend you to read Through the Guide and accurately
              fill in the appliaction form
              <div className="text-danger">
                (Note: This form can not be edited after submiting)
              </div>
            </Typography.Title>
          </Col>
        </Row>
        <div className="my-4">
          <Tabs
            type="card"
            centered
            onChange={handleChange}
            items={[
              {
                label: (
                  <span className="fw-bold text-capitalize px-4">
                    Application form
                  </span>
                ),
                key: "application form",
                children: (
                  <ApplicationForm
                    activeTab={activeTab}
                    setActiveTab={setActiveTab}
                    setContent={setContent}
                  />
                ),
              },
              {
                label: (
                  <span className="fw-bold text-capitalize px-4">
                    Nomination form
                  </span>
                ),
                key: "nomination form",
                children: <NominationForm />,
              },
            ]}
          />
        </div>
      </Card>
    </>
  );
};

export default FormPage;

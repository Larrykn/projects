import React, { useEffect } from "react";
import { useNavigate, redirect } from "react-router-dom";
import { Wrapper, ButtonItem } from "../../../atoms";
import { Footer } from "../../../molecules";
import { Card, Row, Col, Space, Typography } from "antd";

const { Title } = Typography;

const PageNotFound = () => {
  const navigate = useNavigate();

  useEffect(() => {
    setTimeout(sendBack, 1000);
  }, []);

  const sendBack = () => {
    navigate("/");
  };

  return (
    <Wrapper
      display="flex"
      justify="center"
      align="center"
      className="min-vh-100 overflow-auto"
      style={{ backgroundColor: "var(--clr-main)", padding: "10px 0 100px" }}
    >
      <Row>
        <Col>
          <Card
            className="bg-white border"
            style={{
              margin: "0 auto",
              backgroundColor: "transparent",
            }}
          >
            <Space direction="vertical">
              <Title level={2} type="danger">
                Sorry.. Page Not Found
              </Title>
              <ButtonItem
                hasEvent
                htmlType="button"
                text="Back to login"
                block
                handleClick={sendBack}
              />
            </Space>
          </Card>
        </Col>
      </Row>
      <Footer positioned showContact />
    </Wrapper>
  );
};

export default PageNotFound;

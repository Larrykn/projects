import React, { useState } from "react";
import { Layout, Menu, Input } from "antd";
import { DrawerItem, MenuItem, Wrapper } from "../../atoms";
import { Footer, Header } from "../../molecules";
import {
  Profilepage,
  FormPage,
  HomePage,
  VotingPage,
  GuidePage,
  NotificationPage,
} from "../pages/content";
import useWindowSize from "../../../config/hooks/useWindowSize";
import { menuItems } from "../../data";
import { subPathnames } from "../../../config/routes";
import { Routes } from "react-router-dom";

const { Content, Sider } = Layout;

const MainLayout = () => {
  const [content, setContent] = useState({
    mainPath: "home",
    subPath: subPathnames.home.subPath,
    tailPath: subPathnames.home.subPath,
  });
  const [drawerStatus, setDrawerStatus] = useState(false);
  const [collapsible, setCollapsible] = useState(false);

  const { width } = useWindowSize();

  const showDrawer = () => {
    if (width <= 1086) {
      setDrawerStatus(!drawerStatus);
    }
    setCollapsible(!collapsible);
  };

  const handleSelect = ({ key }) => {
    if (key !== "logout") {
      setContent((prevState) => {
        return {
          ...prevState,
          mainPath: key,
          subPath: subPathnames[key]?.subPath || null,
          tailPath: subPathnames[key]?.tailPath || null,
        };
      });
      setDrawerStatus(false);
    } else {
      console.log("user loging out");
    }
  };

  const renderMenuItem = () => {
    switch (content.mainPath) {
      case "home":
        return <HomePage />;
      case "forms":
        return <FormPage content={content} setContent={setContent} />;
      case "vote":
        return <VotingPage content={content} setContent={setContent} />;
      case "guide":
        return <GuidePage setContent={setContent} />;
      case "profile":
        return <Profilepage content={content} setContent={setContent} />;
      case "notifications":
        return <NotificationPage setContent={setContent} />;
      default:
        return <h1>content missing</h1>; // to be changed
    }
  };

  return (
    <Layout className="min-vh-100">
      <Header
        showDrawer={showDrawer}
        handleSelect={handleSelect}
        location={content}
        setContent={setContent}
      >
        <DrawerItem
          title={
            <Input.Search
              placeholder="input search text"
              // onSearch={onSearch}
              enterButton
              allowClear
            />
          }
          displayFor="left"
          open={drawerStatus}
          height={96}
          setOpen={showDrawer}
          items={
            <MenuItem
              defaultSelectedKeys={["home"]}
              defaultActiveFirst={true}
              activeKey={content.mainPath}
              selectedKeys={content.mainPath}
              items={menuItems.sideNavTabs}
              onClick={handleSelect}
            />
          }
        />
      </Header>
      <Layout>
        <Sider
          className=""
          width={250}
          hidden={width <= 1089 ? true : false}
          collapsible
          onChange={showDrawer}
        >
          <div
            className=" position-sticky text-center bg-info text-white"
            style={{ top: 120 }}
          >
            <MenuItem
              defaultSelectedKeys={["home"]}
              defaultActiveFirst={true}
              activeKey={content.mainPath}
              selectedKeys={content.mainPath}
              theme="dark"
              items={menuItems.sideNavTabs}
              onClick={handleSelect}
            />
          </div>
        </Sider>
        <Content>
          <Wrapper display="flex" direction="column">
            <div className="p-3">{renderMenuItem()}</div>
            <div className="p-3">
              <Footer className="mt-3" innerStyle="py-2" />
            </div>
          </Wrapper>
        </Content>
      </Layout>
    </Layout>
  );
};

export default MainLayout;

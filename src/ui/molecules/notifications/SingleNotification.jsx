import React from "react";
import { CardItem, AvatarItem} from "../../atoms";
import img from "../../../assets/images/logo-mobile.svg";
import { MdArrowBack } from "react-icons/md";
import { useState } from "react";

const SingleNotification = ({ message, renderItem }) => {
  
  return (
    <>
      <CardItem>
        <div className="my-4">
          <MdArrowBack
            id="back"
            size={25}
            onClick={renderItem}
            style={{ cursor: "pointer" }}
          />
        </div>
        <div style={{ overFlowY: "none" }}>
          <div className="d-flex gap-3 align-items-center">
            <AvatarItem image={<img src={img} />} />
            <div>
              <div className="d-flex gap-3">
                <span
                  style={{ color: "var(--clr-primary)" }}
                  className="message-author"
                >
                  {message.author}
                </span>
                <span>{message.datetime}</span>
              </div>
            </div>
          </div>
        </div>
        <div className=" rounded my-3 py-4">
          <p className="fw-bold">Dear larry,</p>
          <p>&emsp;&emsp; {message.content.body}</p>
          <p className="pt-4">Thank you,</p>
        </div>
      </CardItem>
    </>
  );
};

export default SingleNotification;

import React from "react";
import { List, Checkbox, Row, Col,Divider } from "antd";
import { Wrapper } from "../../atoms";
import { truncate } from "lodash";
import { nanoid } from "nanoid";
import { useState } from "react";

const NotificationList = ({ list, renderItem, handleChange }) => {
  const renderNotification = e => {
    let id = e?.currentTarget?.id;

    const target = items.find(item => item.key === id);
    if (id) {
      setMessage(prevState => {
        return {
          ...prevState,
          itemId: id,
          type: id === "back" ? "all" : "single",
          item: id === "back" ? items : target
        };
      });
    } else {
      id = "back";
    }
  };
  return (
    <>
      <div>
        <Checkbox indeterminate className="ps-2">
          Select all
        </Checkbox>
        <Divider type="vertical" />
      </div>
      <Divider type="horizontal" />
      <List
        dataSource={list}
        renderItem={item => (
          <li className={`notification ${item.key} my-2 d-flex gap-3 align-items-center`} data-id={item.key} id={item.key}>
            <div key={item.key} id={item.key} data-id={nanoid(7)} className=" border w-100 px-2 " style={{ cursor: "pointer" }}>
              <Wrapper display="flex" gap={20} align="center">
                <div className="">
                  <Checkbox id={item.key} checked={item.isChecked} onChange={e => handleChange(e)} />
                </div>
                <Row
                  align="center w-100"
                  className={`my-2 ${item.isSeen ? "fw-bold" : ""} `}
                  id={item.key}
                  onClick={e => renderItem(e)}
                >
                  <Col md={5} xl={5} className="align-items-center">
                    <span>{item.content.title}</span>
                  </Col>
                  <Col sm={24} md={14} xl={14}>
                    <span className={`mb-0 ${item.isSeen ? "fw-bold" : ""}`}>
                      {truncate(item.content.body, { length: 100 })}
                    </span>
                  </Col>
                  <Col md={5} className="d-flex justify-content-end ">
                    <span className="me-3">{item.datetime}</span>
                  </Col>
                </Row>
              </Wrapper>
            </div>
          </li>
        )}
      />
    </>
  );
};

export default NotificationList;

import React from "react";
import { ButtonItem, Wrapper, DropdownItem, Textdisplay, MiniLogo } from "../../atoms";
import { MenuOutlined } from "@ant-design/icons";
import { Row, Col, Breadcrumb } from "antd";
import useWindowSize from "../../../config/hooks/useWindowSize";
import { FiMoreVertical } from "react-icons/fi";

const Header = ({
  showDrawer,
  children,
  handleSelect,
  location,
  setContent,
}) => {
  const handleClick = () => {
    setContent((prevState) => {
      return {
        ...prevState,
        mainPath: "home",
        subPath: null,
        tailPath: null,
      };
    });
  };
  const { width } = useWindowSize();

  return (
    <>
      <header className="position-sticky top-0" style={{ zIndex: 1000 }}>
        <div
          className="site-header-content bg-white border px-2"
          style={{ marginBottom: 0.5 }}
        >
          <Row className="">
            <Col
              xs={6}
              md={6}
              lg={8}
              className="d-flex align-items-center  px-3 py-1"
            >
              { width < 1086 ? <ButtonItem
                type="secondary"
                hasEvent
                icon={<MenuOutlined />}
                handleClick={showDrawer}
              /> 
              :<MiniLogo />}
            </Col>
            <Col
              xs={12}
              md={12}
              lg={8}
              className="d-flex align-items-center justify-content-center py-1"
            >
              <div
                className="fs-6 d-flex align-items-center text-uppercase fw-bolder text-muted"
                style={{ cursor: "pointer", color: "var(--clr-primary)" }}
                onClick={handleClick}
              >
                { width <1086? <MiniLogo />
                :<span
                  className={`${width < 1086 ? "d-none " : "block"} `}
                >
                  students online voting system
                </span>}
              </div>
            </Col>
            <Col
              xs={6}
              md={6}
              lg={8}
              className="d-flex align-items-center justify-content-end px-3"
            >
              <DropdownItem handleSelect={handleSelect} />
            </Col>
          </Row>
        </div>
        <Wrapper
          className="border py-1 px-3 "
          style={{ backgroundColor: "#f9f9fb" }}
        >
          <Breadcrumb separator=">">
            <Breadcrumb.Item className="fw-bold">
              {location?.mainPath?.toUpperCase()}
            </Breadcrumb.Item>
            <Breadcrumb.Item className="fw-bold text-capitalize">
              {location?.subPath}
            </Breadcrumb.Item>
            <Breadcrumb.Item className="fw-bold text-capitalize">
              {location?.tailPath}
            </Breadcrumb.Item>
          </Breadcrumb>
        </Wrapper>
        <div className="site-header-extra-content">{children}</div>
      </header>
    </>
  );
};

export default Header;

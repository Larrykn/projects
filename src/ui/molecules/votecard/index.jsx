import React from "react";
import { Card, Row, Col, Image, Radio, Typography, Badge } from "antd";
import { fallbackImage } from "../../data/forms/fallbackimage";
import { NoData, Wrapper, ButtonItem, RadioInput } from "../../atoms";
import { FaVoteYea } from "react-icons/fa";

const VoteCard = ({ data, ...rest }) => {
  if (data.length === 0)
    return (
      <NoData
        description={
          <Typography.Paragraph className="text-uppercase fw-bold text-danger">
            sorry.. no applicants in this category
          </Typography.Paragraph>
        }
        width="100%"
      />
    );
  return (
    <>
      {data.map((item, index) => (
        <Col
          xs={24}
          md={24}
          lg={12}
          xl={12}
          xxl={12}
          key={`applicant-${index + 1}`}
        >
          <Badge.Ribbon text={<span className="fw-bold">{item.party}</span>}>
            <Card bodyStyle={{ padding: "12px" }}>
              <Row gutter={[10, 10]}>
                <Col
                  xs={24}
                  sm={12}
                  md={12}
                  xl={12}
                  xxl={12}
                  className="d-flex justify-content-center"
                >
                  <Image
                    height={180}
                    width={180}
                    src="error"
                    fallback={fallbackImage}
                  />
                </Col>
                <Col xs={24} sm={12} md={12} xl={12} xxl={12} className="py-3">
                  <Wrapper display="flex" direction="column" gap={20}>
                    <div>
                      <div className="">
                        <span className="fs-6 text-uppercase fw-bold text-muted ">
                          first name : {item.firstName}
                        </span>
                      </div>
                      <div className="">
                        <span className="fs-6 text-uppercase fw-bold text-muted">
                          other names : {item.otherNames}
                        </span>
                      </div>
                      <div className="">
                        <span className="fs-6 text-uppercase fw-bold text-muted">
                          reg. no : {item.RegNo}
                        </span>
                      </div>
                      <div className="">
                        <span className="fs-6 text-uppercase fw-bold text-muted ">
                          Gender: {item.gender}
                        </span>
                      </div>
                    </div>

                    <Wrapper className="mt-auto">
                      <Radio.Button
                        value={`${item.firstName} ${item.otherNames}`}
                      >
                        <Wrapper
                          className="applicant-selection-button d-flex fw-bold"
                          align="center"
                          gap={10}
                        >
                          <FaVoteYea />
                          <span>vote</span>
                        </Wrapper>
                      </Radio.Button>
                    </Wrapper>
                  </Wrapper>
                </Col>
              </Row>
            </Card>
          </Badge.Ribbon>
        </Col>
      ))}
    </>
  );
};

VoteCard.defaultProps = {
  data: [],
};
export default VoteCard;

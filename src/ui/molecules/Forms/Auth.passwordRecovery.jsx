import React from "react";
import { Wrapper, TextInput, ButtonItem } from "../../atoms";
import { AiOutlineMail } from "react-icons/ai";
import { Form } from "antd";

const PasswordRecovery = () => {
  const handleFinish = (values) => {
    console.log(values);
  };
  return (
    <>
      <Form
        layout="vertical"
        requiredMark={false}
        onFinish={(values) => handleFinish(values)}
      >
        <TextInput
          name="email"
          htmlFor="email"
          label={<span className="text-muted">Email</span>}
          tooltip="This is a required field"
          rules={[
            {
              required: true,
              message: "please enter your email address",
            },
          ]}
          prefix={<AiOutlineMail />}
          placeholder="Enter your email"
        />
        <ButtonItem htmlType="submit" text="Register" type="primary" block />
        <Wrapper
          className="form-sections-buttons mt-2"
          display="flex"
          justify="center"
        >
          <span
            style={{ color: "#1890ff", cursor: "pointer" }}
            role="button"
            onClick={() => console.log("back to login")}
          >
            Back to login
          </span>
        </Wrapper>
      </Form>
    </>
  );
};

export default PasswordRecovery;

import React from "react";
import { Wrapper, TextInput, ButtonItem } from "../../atoms";
import { AiOutlineMail } from "react-icons/ai";
import { LockOutlined } from "@ant-design/icons";
import { Form } from "antd";

const Registration = (values) => {
  const handleFinish = (values) => {
    console.log(values);
  };
  const handleClick = (e) => {
    console.log(e, "clicked");
  };
  return (
    <>
      <Form
        layout="vertical"
        requiredMark={false}
        onFinish={(values) => handleFinish(values)}
      >
        <TextInput
          name="fullname"
          htmlFor="fullname"
          label={<span className="text-muted">Full Name</span>}
          tooltip="This is a required field"
          rules={[
            {
              required: true,
              message: "please enter your first name",
            },
          ]}
          prefix={<AiOutlineMail />}
          placeholder="Enter your name"
        />
        <TextInput
          name="email"
          htmlFor="email"
          label={<span className="text-muted">Email</span>}
          tooltip="This is a required field"
          rules={[
            {
              required: true,
              message: "please enter your email address",
            },
          ]}
          prefix={<AiOutlineMail />}
          placeholder="Enter your email"
        />
        <TextInput
          name="password"
          htmlFor="password"
          label={<span className="text-muted">Password</span>}
          tooltip="This is a required field"
          rules={[
            {
              required: true,
              message: "please enter your password",
            },
          ]}
          prefix={<LockOutlined />}
          type="password"
          placeholder="Enter your password"
        />
        <ButtonItem
          htmlType="submit"
          text="Register"
          type="primary"
          block
          onClick={handleClick}
        />
        <Wrapper
          className="form-sections-buttons mt-2"
          display="flex"
          justify="center"
        >
          <span
            style={{ color: "#1890ff", cursor: "pointer" }}
            role="button"
            onClick={() => console.log("i have an account")}
          >
            i have an account
          </span>
        </Wrapper>
      </Form>
    </>
  );
};

export default Registration;

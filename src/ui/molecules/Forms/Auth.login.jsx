import React from "react";
import { Wrapper, TextInput, ButtonItem } from "../../atoms";
import { AiOutlineMail } from "react-icons/ai";
import { Form } from "antd";
import { LockOutlined } from "@ant-design/icons";
import { FiLogIn } from "react-icons/fi";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const handleFinish = () => {
    console.log("hello hey");
  };

  return (
    <>
      <Form
        layout="vertical"
        requiredMark={false}
        onFinish={(values) => handleFinish(values)}
      >
        <TextInput
          name="email"
          htmlFor="email"
          label={<span className="text-muted">Email</span>}
          tooltip="This is a required field"
          rules={[
            {
              required: true,
              message: "please enter your email address",
            },
          ]}
          prefix={<AiOutlineMail />}
          placeholder="Enter your email"
        />
        <TextInput
          name="password"
          htmlFor="password"
          label={<span className="text-muted">Password</span>}
          tooltip="This is a required field"
          rules={[
            {
              required: true,
              message: "please enter your password",
            },
          ]}
          prefix={<LockOutlined />}
          type="password"
          placeholder="Enter your password"
        />
        <ButtonItem
          htmlType="submit"
          text="Login"
          type="primary"
          block
          icon={<FiLogIn />}
        />
        <Wrapper
          className="form-sections-buttons mt-2"
          display="flex"
          justify="space-between"
        >
          <span
            style={{ color: "#1890ff", cursor: "pointer" }}
            role="button"
            onClick={() => handleNavigation()}
          >
            I don't have an account
          </span>
          <span
            style={{ color: "#1890ff", cursor: "pointer" }}
            role="button"
            onClick={() => console.log("password-recovery")}
          >
            Forgot password
          </span>
        </Wrapper>
      </Form>
    </>
  );
};

export default Login;

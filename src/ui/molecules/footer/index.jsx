import React from "react";
import { Row, Col } from "antd";

const Footer = ({ showContact, positioned, style, className, innerStyle }) => {
  return (
    <>
      <footer
        className={`border ${
          positioned ? "fixed-bottom" : ""
        } bg-white ${className}`}
        style={style}
      >
        <Row className={`p-2 text-center ${innerStyle}`}>
          <Col xs={24} lg={positioned ? 12 : 24}>
            <small className="text-muted">
              copyright&copy; 2022 students voting system. ALl rights reverved
            </small>
          </Col>
          {showContact && (
            <Col xs={24} lg={12}>
              <small className="text-muted">
                contact us. help.studentsvotingsystem@gmail.com
              </small>
            </Col>
          )}
        </Row>
      </footer>
    </>
  );
};

Footer.defaultProps = {
  className: "",
  showContact: false,
  positioned: false,
};

export default Footer;

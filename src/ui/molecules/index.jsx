export { default as PageLoader } from "./pageLoader/index";
export { default as Footer } from "./footer/index";
export { default as Login } from "./Forms/Auth.login";
export { default as Registration } from "./Forms/Auth.resgistration";
export { default as PasswordRecovery } from "./Forms/Auth.passwordRecovery";
export { default as Header } from "./header/Header";
export { default as SingleNotification } from "./notifications/SingleNotification";
export { default as NotificationList } from "./notifications/NotificationList";

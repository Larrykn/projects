import React from "react";
import { Wrapper, DataSpinner } from "../../atoms";

const PageLoader = ({ text }) => {
  return (
    <>
      <Wrapper
        className="main-application-page-loader content-wrapper p-2"
        justify="center"
        align="center"
        display="flex"
        height="100vh"
        style={{ backgroundColor: "var(--clr-main)" }}
      >
        <div className="text-center">
          <DataSpinner width={80} height={80} />
          <span className="fw-bold text-secondary">{text}</span>
        </div>
      </Wrapper>
    </>
  );
};

PageLoader.defaultProps = {
  text: "Loading...",
};

export default PageLoader;

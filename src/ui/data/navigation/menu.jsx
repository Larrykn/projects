import { FaVoteYea, FaSignOutAlt } from "react-icons/fa";
import {
  HomeOutlined,
  FormOutlined,
  UserOutlined,
  FileTextOutlined,
  MailOutlined,
  NotificationFilled,
} from "@ant-design/icons";
import { Badge } from "antd";
import { BadgeItem } from "../../atoms";

const menuItems = {
  sideNavTabs: [
    {
      navid: "1",
      label: "Home",
      key: "home",
      icon: <HomeOutlined />,
    },
    {
      navid: "2",
      label: "Forms",
      key: "forms",
      icon: <FormOutlined />,
    },
    {
      navid: "3",
      label: "Vote",
      key: "vote",
      icon: <FaVoteYea />,
    },
    {
      navid: "4",
      label: "Guide",
      key: "guide",
      icon: <FileTextOutlined />,
    },
    {
      navid: "5",
      label: "Profile",
      key: "profile",
      icon: <UserOutlined />,
    },
    {
      icon: <NotificationFilled />,
      label: "Notification(s)",
      key: "notifications",
    },
  ],
};

export default menuItems;

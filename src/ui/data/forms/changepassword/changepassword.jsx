export const changePassword = [
  {
    name: "currentpassword",
    label: "current password",
    htmlFor: "password",
    rules: [
      {
        required: true,
        message: "Please enter your current password",
      },
    ],
    hasFeedback: true,
  },
  {
    name: "newPassword",
    label: "new password",
    htmlFor: "password",
    rules: [
      {
        required: true,
        message: "Please enter your new password",
      },
    ],
    hasFeedback: true,
  },
  {
    name: "confirm",
    label: "confirm password",
    htmlFor: "password",
    dependencies: ["password"],
    rules: [
      {
        required: true,
        message: "Please confirm your password",
      },
      ({ getFieldValue }) => ({
        validator(_, value) {
          if (!value || getFieldValue("newPassword") === value) {
            return Promise.resolve();
          }

          return Promise.reject(
            new Error("The two passwords that you entered do not match!")
          );
        },
      }),
    ],
    hasFeedback: true,
  },
];

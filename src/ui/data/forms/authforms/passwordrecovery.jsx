import { AiOutlineMail } from "react-icons/ai";

const passwordrecovery = [
  {
    name: "email",
    label: "Email",
    htmlFor: "email",
    tooltip: "This is a required field",
    rules: [
      {
        required: true,
        message: "please enter your email address",
      },
    ],
    prefix: <AiOutlineMail />,
    placeholder: "Enter your password",
  },
];

export default passwordrecovery;

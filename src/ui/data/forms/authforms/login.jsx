import { AiOutlineMail } from "react-icons/ai";
import { LockOutlined } from "@ant-design/icons";

const login = [
  {
    name: "email",
    htmlFor: "email",
    label: "email",
    tooltip: "This is a required field",
    rules: [
      {
        required: true,
        message: "please enter your email address",
      },
    ],
    prefix: <AiOutlineMail />,
    placeholder: "Enter your email",
  },
  {
    name: "password",
    htmlFor: "password",
    label: "password",
    tooltip: "This is a required field",
    rules: [
      {
        required: true,
        message: "please enter your password",
      },
    ],
    prefix: <LockOutlined />,
    placeholder: "Enter your password",
  },
];

export default login;

import { AiOutlineMail } from "react-icons/ai";
import { LockOutlined, UserOutlined } from "@ant-design/icons";

const resgistration = [
  {
    name: "fullname",
    htmlFor: "fullname",
    label: "full name",
    tooltip: "This is a required field",
    rules: [
      {
        required: true,
        message: "please enter your full name",
      },
    ],
    prefix: <UserOutlined />,
    placeholder: "Enter your full name",
  },
  {
    name: "email",
    htmlFor: "email",
    label: "email",
    tooltip: "This is a required field",
    rules: [
      {
        required: true,
        message: "please enter your email address",
      },
    ],
    prefix: <AiOutlineMail />,
    placeholder: "Enter your email",
  },
  {
    name: "password",
    htmlFor: "password",
    label: "password",
    tooltip: "This is a required field",
    rules: [
      {
        required: true,
        message: "please enter your password",
      },
    ],
    prefix: <LockOutlined />,
    placeholder: "Enter your password",
  },
];

export default resgistration;

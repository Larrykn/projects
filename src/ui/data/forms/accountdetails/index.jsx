export const accountdetails = [
  {
    input: "text",
    name: "firstname",
    label: "first name",
    disabled: true,
    rules: [
      {
        required: true,
        message: "please enter your name",
      },
    ],
  },
  {
    input: "text",
    name: "othernames",
    label: "other names",
    disabled: true,
    rules: [
      {
        required: true,
        message: "please enter your other names",
      },
    ],
  },
  {
    input: "text",
    name: "tel",
    label: "tel. no",
    disabled: false,
    rules: [
      {
        required: true,
        message: "please enter your telephone number",
      },
    ],
  },
  {
    input: "text",
    name: "email",
    label: "email",
    disabled: false,
    rules: [
      {
        required: true,
        message: "please enter your email",
      },
    ],
  },
];

import { UserOutlined } from "@ant-design/icons";
const personaldetails = [
  {
    input: "select",
    name: "salutation",
    label: "salutation",
    options: ["Dr", "Eng", "Mr", "Mrs"],
    rules: [
      {
        required: true,
        message: "please select an option",
      },
    ],

    prefix: null,
  },
  {
    input: "text",
    name: "name",
    label: "Name (first, middle, last)",
    rules: [
      {
        required: true,
        message: "please enter your name",
      },
    ],

    placeholder: "Enter your name",
    prefix: null,
    tooltip: "Please enter your name in the order.",
  },

  {
    input: "text",
    name: "email",
    label: "email",
    rules: [
      {
        required: true,
        message: "please enter your email",
      },
    ],

    placeholder: "Enter your email",
    prefix: null,
  },
  {
    input: "phone",
    name: "phone",
    label: "Tel.No",
    rules: [
      {
        required: true,
        message: "please enter your Telephone number",
      },
    ],

    placeholder: "Enter your Telephone number",
    prefix: null,
  },
  {
    input: "select",
    name: "gender",
    label: "gender",
    options: ["male", "female"],
    rules: [
      {
        required: true,
        message: "please select your gender",
      },
    ],

    placeholder: "--select--",
  },
  {
    input: "date",
    name: "dateOfBirth",
    label: "date of birth (D.O.B)",
    rules: [
      {
        required: true,
        message: "please select Date of Birth",
      },
    ],
    placeholder: "--select--",
  },
];

export default personaldetails;

const contactdetails = [
  {
    input: "text",
    name: "district",
    label: "District",
    rules: [
      {
        required: true,
        message: "please enter your district",
      },
    ],
    placeholder: "Enter your district",
  },
  {
    input: "text",
    name: "address",
    label: "Address",
    rules: [
      {
        required: true,
        message: "please enter your address",
      },
    ],
    placeholder: "Enter your address",
  },
  {
    input: "text",
    name: "fathersname",
    label: "Father's name",
    rules: [
      {
        required: true,
        message: "please enter your father's name",
      },
    ],
    placeholder: "Enter your father's name",
  },
  {
    input: "text",
    name: "mothersname",
    label: "Mother's name",
    rules: [
      {
        required: true,
        message: "please enter your father's name",
      },
    ],
    placeholder: "Enter your mother's name",
  },
  {
    input: "text",
    name: "fatherscontact",
    label: "Father's contact",
    rules: [
      {
        required: true,
        message: "please enter your father's contact",
      },
    ],
    placeholder: "Enter your father's contact",
  },
  {
    input: "text",
    name: "motherscontact",
    label: "Mother's contact",
    rules: [
      {
        required: true,
        message: "please enter your mother's contact",
      },
    ],
    placeholder: "Enter your mother's contact",
  },
];

export default contactdetails;

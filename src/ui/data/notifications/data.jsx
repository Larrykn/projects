const data = [
  {
    key: "1",
    actions: [],
    author: "support@Studentsvotingsystem",
    isSeen: false,
    isChecked: true,
    content: {
      title: "User notification 1",
      body: ` Dear user this is a testing notifications sent by the IT support team
          of students voting system, please don't reply to this messages. Incase
          you are interested in giving us a feedback, please feel free to send
          us an email at studentsvotingsystem.support@gmail.com`,
    },
    datetime: "8 hours ago",
  },
  {
    key: "2",
    actions: [],
    author: "support@Studentsvotingsystem",

    isSeen: true,
    isChecked: false,
    content: {
      title: "User notification 2",
      body: ` Dear user this is a testing notifications sent by the IT support team
          of students voting system, please don't reply to this messages. Incase
          you are interested in giving us a feedback, please feel free to send
          us an email at studentsvotingsystem.support@gmail.com`,
    },
    datetime: "9 hours ago",
  },
  {
    key: "3",
    actions: [],
    author: "support@Studentsvotingsystem",

    isSeen: true,
    isChecked: false,
    content: {
      title: "User notification 3",
      body: ` Dear user this is a testing notifications sent by the IT support team
          of students voting system, please don't reply to this messages. Incase
          you are interested in giving us a feedback, please feel free to send
          us an email at studentsvotingsystem.support@gmail.com`,
    },
    datetime: "9 hours ago",
  },
  {
    key: "4",
    actions: [],
    author: "support@Studentsvotingsystem",

    isSeen: true,
    isChecked: false,
    content: {
      title: "User notification 4",
      body: ` Dear user this is a testing notifications sent by the IT support team
          of students voting system, please don't reply to this messages. Incase
          you are interested in giving us a feedback, please feel free to send
          us an email at studentsvotingsystem.support@gmail.com`,
    },
    datetime: "9 hours ago",
  },
  {
    key: "5",
    actions: [],
    author: "support@Studentsvotingsystem",

    isSeen: true,
    isChecked: false,
    content: {
      title: "User notification 5",
      body: ` Dear user this is a testing notifications sent by the IT support team
          of students voting system, please don't reply to this messages. Incase
          you are interested in giving us a feedback, please feel free to send
          us an email at studentsvotingsystem.support@gmail.com`,
    },
    datetime: "9 hours ago",
  },
  {
    key: "6",
    actions: [],
    author: "support@Studentsvotingsystem",

    isSeen: true,
    isChecked: false,
    content: {
      title: "User notification 6",
      body: ` Dear user this is a testing notifications sent by the IT support team
          of students voting system, please don't reply to this messages. Incase
          you are interested in giving us a feedback, please feel free to send
          us an email at studentsvotingsystem.support@gmail.com`,
    },
    datetime: "10 hours ago",
  },
  {
    key: "7",
    actions: [],
    author: "support@Studentsvotingsystem",

    isSeen: true,
    isChecked: false,
    content: {
      title: "User notification 7",
      body: ` Dear user this is a testing notifications sent by the IT support team
          of students voting system, please don't reply to this messages. Incase
          you are interested in giving us a feedback, please feel free to send
          us an email at studentsvotingsystem.support@gmail.com`,
    },
    datetime: "11 hours ago",
  },
];

export default data;

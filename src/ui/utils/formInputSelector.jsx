import React from "react";
import { TextInput, SelectItem, DateInput, PhoneInput } from "../atoms";
import { Col } from "antd";

const formInputSelector = async (items) => {
  const data = await items.map((item, i) => {
    try {
      switch (item.input) {
        case "text":
          return (
            <Col xs={24} xl={12} key={i}>
              <TextInput {...item} />
            </Col>
          );
        case "select":
          return (
            <Col xs={24} xl={12} key={i}>
              <SelectItem {...item} />
            </Col>
          );
        case "date":
          return (
            <Col xs={24} xl={12} key={i}>
              <DateInput {...item} />
            </Col>
          );
        case "phone":
          return (
            <Col xs={24} xl={12} key={i}>
              <PhoneInput {...item} />
            </Col>
          );
        default:
          return (
            <Col xs={24} xl={12} key={i}>
              <TextInput {...item} />
            </Col>
          );
      }
    } catch (err) {
      return err.message;
    }
  });

  return data;
};
export default formInputSelector;

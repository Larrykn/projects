import React from "react";
import { Badge } from "antd";

const BadgeItem = ({ children, size, dot, showBadge }) => {
  const msgCount = 7;
  return (
    <>
      <Badge dot={dot} count={dot ? null : msgCount} size={size}>
        {children}
      </Badge>
    </>
  );
};

BadgeItem.defaultProps = {
  size: "small",
};
export default BadgeItem;

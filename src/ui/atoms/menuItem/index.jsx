import { Menu } from "antd";
import PropTypes from "prop-types";

import React from "react";

const MenuItem = ({ items, ...rest }) => {
  return <Menu items={items} {...rest} />;
};

export default MenuItem;

MenuItem.defaultProps = {
  items: [],
};

MenuItem.propTypes = {
  items: PropTypes.array,
};

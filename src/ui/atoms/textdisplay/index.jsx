import React from "react";
import useWindowSize from "../../../config/hooks/useWindowSize";

const Textdisplay = ({ text }) => {
  const width = useWindowSize();

  return (
    <>
      <span
        className={`${width < 1086 ? "d-none " : "block"}  px-2`}
        style={{ backgroundColor: "var(--clr-primary)" }}
      >
        {text}
      </span>
    </>
  );
};

export default Textdisplay;

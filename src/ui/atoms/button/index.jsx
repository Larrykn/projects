import React from "react";
import { PropTypes } from "prop-types";
import { Button } from "antd";

const ButtonItem = ({
  hasEvent,
  icon,
  text,
  block,
  style,
  size,
  type,
  handleClick,
  htmlType,
  actionFor,
  width,
  ...rest
}) => {
  const shouldHandleEventHandler = hasEvent;

  return (
    <>
      <Button
        data-id={actionFor}
        size={size}
        name={text}
        style={{ ...style, width: `${width}px` }}
        type={type}
        block={block}
        htmlType={htmlType}
        className="d-flex align-items-center justify-content-center gap-2"
        icon={icon}
        onClick={shouldHandleEventHandler ? (e) => handleClick(e) : undefined}
        {...rest}
      >
        {text}
      </Button>
    </>
  );
};
ButtonItem.defaultProps = {
  block: false,
  type: "primary",
  text: "",
  htmlType: "button",
  hasEvent: false,
  actionFor: "menu",
  width: "",
  size: "medium",
};
ButtonItem.propTypes = {
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  icon: PropTypes.node,
  text: PropTypes.node,
  block: PropTypes.bool,
  style: PropTypes.object,
  type: PropTypes.string,
  handleClick: PropTypes.func,
  htmlType: PropTypes.string,
  hasEvent: PropTypes.bool,
  actionFor: PropTypes.string,
  size: PropTypes.string,
};
export default ButtonItem;

import React from "react";
import { Empty } from "antd";
import Wrapper from "../wrapper";

const NoData = ({ description, ...rest }) => {
  return (
    <>
      <Wrapper className="site-no-data-container" {...rest}>
        <Empty description={description} />
      </Wrapper>
    </>
  );
};

export default NoData;

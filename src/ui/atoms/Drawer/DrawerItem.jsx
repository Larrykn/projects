import React from "react";
import { Drawer } from "antd";
import AvatarItem from "../avatar/AvatarItem";
import Wrapper from "../wrapper";
import ButtonItem from "../button";
const DrawerItem = ({
  setOpen,
  children,
  height,
  open,
  items,
  displayFor,
  title,
}) => {
  return (
    <>
      <Drawer
        theme="dark"
        placement={displayFor}
        title={title}
        footer={
          <small className="text-muted">
            copyright&copy; 2022 students voting system
          </small>
        }
        drawerStyle={{
          backgroundColor: "var(--bg-white)",
        }}
        bodyStyle={{ padding: 0 }}
        open={open}
        closable={false}
        onClose={() => setOpen(false)}
        footerStyle={{ display: "flex", justifyContent: "center" }}
        zIndex={900}
        contentWrapperStyle={{ margin: `${height - 2}px 0 0 ` }}
      >
        {items}
        {children}
      </Drawer>
    </>
  );
};

export default DrawerItem;

import { Dropdown, Badge } from "antd";
import ButtonItem from "../button";
import AvatarItem from "../avatar/AvatarItem";
import MenuItem from "../menuItem";
import BadgeItem from "../badge";
import { UserOutlined, MailOutlined } from "@ant-design/icons";
import { FaSignOutAlt } from "react-icons/fa";
import useWindowSize from "../../../config/hooks/useWindowSize";
import { useState } from "react";

const DropdownItem = ({ handleSelect }) => {
  const [showBadge, setShowBadge] = useState(true);
  const { width } = useWindowSize();
  const userMenu = [
    {
      icon: <UserOutlined />,
      label: "Profile",
      key: "profile",
    },
    {
      icon: (
        <BadgeItem>
          <MailOutlined />
        </BadgeItem>
      ),
      label: <span className="ms-2">Notifications(s)</span>,
      key: "notifications",
    },
    {
      icon: <FaSignOutAlt />,
      label: "Logout",
      key: "logout",
    },
  ];

  const handleChange = () => {
    setShowBadge(!showBadge);
    console.log(showBadge);
  };

  return (
    <Dropdown
      overlay={<MenuItem items={userMenu} onClick={handleSelect} />}
      trigger="click"
      onOpenChange={handleChange}
      arrow
    >
      <ButtonItem
        type="secondary"
        className="d-flex border-0 align-items-center gap-2 py-3"
        text={
          width > 1000 ? (
            <span className="fw-bold text-muted d-flex">LARRY</span>
          ) : null
        }
        icon={
          <BadgeItem count={showBadge ? 7 : null}>
            <AvatarItem />
          </BadgeItem>
        }
      />
    </Dropdown>
  );
};

export default DropdownItem;

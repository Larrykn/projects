import React from "react";
import { Card } from "antd";

const CardItem = ({ children, title, extra, ...props }) => {
  return (
    <Card title={title} extra={extra} {...props}>
      {children}
    </Card>
  );
};

export default CardItem;

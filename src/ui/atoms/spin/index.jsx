import React from "react";
import { Oval } from "react-loader-spinner";

const DataSpinner = ({ height, width }) => {
  return (
    <Oval
      height={height}
      width={width}
      color="#5c636a"
      wrapperClass="page-loader-container"
      visible={true}
      ariaLabel="oval-loading"
      secondaryColor="#5c636a"
      strokeWidth={2}
      strokeWidthSecondary={2}
    />
  );
};
Oval.defaultProps = {
  width: 20,
  height: 20,
};
export default DataSpinner;

import React from "react";
import Logo from "../logo";
import { Typography } from "antd";

const DocumentHeader = ({ children, textFor }) => {
  return (
    <>
      <div className={`document-header`}>
        <Logo width={150} />
        <Typography.Title level={3} className="text-uppercase ">
          <span
            className="text-white px-1"
            style={{ backgroundColor: "var(--clr-primary)" }}
          >
            {`Students voting system ${textFor}`}
          </span>
        </Typography.Title>
        {children}
      </div>
    </>
  );
};

export default DocumentHeader;

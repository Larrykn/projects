import React from "react";
import PropTypes from "prop-types";

const Wrapper = ({
  children,
  width,
  gap,
  justify,
  direction,
  align,
  height,
  display,
  className,
  style,
}) => {
  return (
    <div
      className={className}
      style={{
        display,
        flexDirection: direction,
        justifyContent: justify,
        alignItems: align,
        width,
        height,
        gap,
        ...style,
      }}
    >
      {children}
    </div>
  );
};
Wrapper.defaultProps = {
  width: "auto",
  justifyItems: "flex-start",
  alignItems: "flex-start",
  flexDirection: "row",
  gap: "0",
  height: "auto",
  display: "block",
  className: "Content-wrapper",
  style: {},
};
Wrapper.propTypes = {
  children: PropTypes.node,
  flexDirection: PropTypes.string,
  gap: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  justifyItems: PropTypes.string,
  alignItems: PropTypes.string,
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  display: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object,
};
export default Wrapper;

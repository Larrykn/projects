import React from "react";
import { Avatar } from "antd";
import { UserOutlined } from "@ant-design/icons";

const AvatarItem = ({ size, image }) => {
  return (
    <>
      <Avatar
        size={size}
        icon={image ? image : <UserOutlined />}
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      />
    </>
  );
};

Avatar.defaultProps = {
  size: 35,
};
export default AvatarItem;

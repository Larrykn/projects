import React from "react";
import logo from "../../../assets/images/logo-mobile.svg";

const MiniLogo = () => {
  return (
    <>
      <div className=" px-3 py-1 d-flex align-items-center ">
        <img
          src={logo}
          style={{
            width: 40
          }}
          className="mobile-logo"
        />
      </div>
    </>
  );
};

export default MiniLogo;

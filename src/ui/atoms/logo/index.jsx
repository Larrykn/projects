import React from "react";
import Wrapper from "../wrapper";
import img from "../../../assets/images/site-logo-large.svg";
import img2 from "../../../assets/images/site-logo-large-white.svg";

const Logo = ({ width, color }) => {
  return (
    <>
      <Wrapper
        display="flex"
        justify="center"
        className="application-logo mb-2"
      >
        <img
          src={color ? img2 : img}
          alt="logo"
          style={{ width: `${width}px`, margin: "0 20px", color: `${color}` }}
        />
      </Wrapper>
    </>
  );
};

export default Logo;

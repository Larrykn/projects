import React, { useState } from "react";
import { Form, Input, Select } from "antd";
import { PropTypes } from "prop-types";
import { countries } from "../../data";
import ButtonItem from "../button";
import Wrapper from "../wrapper";

const { Option } = Select;

const PhoneInput = () => {
  const [phoneNumber, setPhoneNumber] = useState({ phone: "", error: "" });

  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select
        optionLabelProp="label"
        style={{ width: 100 }}
        defaultValue="+256"
        dropdownMatchSelectWidth={false}
      >
        {countries.map((country, i) => (
          <Option key={i} value={country.phoneCode} label={country.phoneCode}>
            <Wrapper display="flex" gap={5}>
              <span>{country.countryName}</span>
              {`( ${country.phoneCode} )`}
            </Wrapper>
          </Option>
        ))}
      </Select>
    </Form.Item>
  );
  return (
    <>
      <Form.Item
        name="phone"
        label="Tel.No"
        rules={[
          {
            required: true,
            message: "please enter your phone number",
          },
        ]}
      >
        <Input addonBefore={prefixSelector} maxLength={13} />
      </Form.Item>
    </>
  );
};
PhoneInput.defaultProps = {
  countries: [],
};

PhoneInput.propTypes = {
  countries: PropTypes.array,
};

export default PhoneInput;

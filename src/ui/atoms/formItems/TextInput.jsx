import React from "react";
import { PropTypes } from "prop-types";
import { Form, Input } from "antd";

const TextInput = ({
  name,
  htmlFor,
  label,
  style,
  tooltip,
  rules,
  bodyClass,
  itemClass,
  prefix,
  placeholder,
  dependencies,

  ...rest
}) => {
  const checkType = () => {
    switch (htmlFor) {
      case "password":
        return (
          <Input.Password
            prefix={prefix}
            placeholder={placeholder}
            className={itemClass}
          />
        );
      default:
        return (
          <Input
            prefix={prefix}
            placeholder={placeholder}
            className={itemClass}
            {...rest}
          />
        );
    }
  };
  return (
    <>
      <Form.Item
        className={bodyClass}
        name={name}
        htmlFor={htmlFor}
        label={<span className="text-capitalize">{label}</span>}
        style={{ marginBottom: "15px", ...style }}
        tooltip={tooltip}
        dependencies={dependencies}
        rules={rules}
        {...rest}
      >
        {checkType()}
      </Form.Item>
    </>
  );
};
TextInput.defau = {
  name: "",
  htmlFor: "",
  label: "",
  style: {},
  tooltip: "This is a required field",
  rules: [],
  bodyClass: "",
  itemClass: "",
  prefix: "",
  placeholder: "",
  dependencies: [],
  disabled: false,
};
TextInput.propTypes = {
  disabled: PropTypes.bool,
  name: PropTypes.string,
  htmlFor: PropTypes.string,
  label: PropTypes.node,
  style: PropTypes.object,
  tooltip: PropTypes.string,
  rules: PropTypes.array,
  bodyClass: PropTypes.string,
  itemClass: PropTypes.string,
  prefix: PropTypes.node,
  placeholder: PropTypes.string,
};

export default TextInput;

import React from "react";
import { Select, Form } from "antd";

const SelectItem = ({ style, rules, label, options, allowClear, ...rest }) => {
  return (
    <Form.Item
      label={<span className="text-capitalize">{label}</span>}
      {...rest}
      style={{ marginBottom: "15px", ...style }}
      rules={rules}
    >
      <Select allowClear={allowClear} placeholder="--select--">
        {options.map((option) => (
          <Select.Option key={option} value={option}>
            <span className="text-capitalize"> {option}</span>
          </Select.Option>
        ))}
      </Select>
    </Form.Item>
  );
};

export default SelectItem;

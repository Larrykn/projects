import React from "react";
import { Form, DatePicker } from "antd";

const DateInput = ({ name, label, ...rest }) => {
  return (
    <Form.Item
      name={name}
      label={<span className="text-capitalize">{label}</span>}
      {...rest}
    >
      <DatePicker />
    </Form.Item>
  );
};

export default DateInput;

import React from "react";
import Wrapper from "../wrapper";
import { Radio } from "antd";

const RadioInput = ({ children, buttonStyle, ...rest }) => {
  return (
    <>
      <Radio.Group buttonStyle={buttonStyle} {...rest}>
        {children}
      </Radio.Group>
    </>
  );
};
RadioInput.defaultProps = {
  buttonStyle: "solid",
  defaultValue: "",
};
export default RadioInput;

const auth = {
  isAuthenticated: false,

  loginIn: false,
  loginData: {},
  loginError: {},

  regitering: false,
  regiteredUser: {},
  regiterError: {},

  logginOut: false,
  logoutData: {},
  alogoutError: {},

  gettingAuthUser: false,
  authUser: {},
  authUserError: {},

  requesting: false,
  verifying: false,
  verifyEmailSuccess: {},
  verifyEmailEmail: {},

  changingPassword: false,
  changingPasswordSuccess: {},
  changingPasswordError: {},

  changingDefaultPassword: false,
  changingDefaultPasswordSuccess: {},
  changingDefaultPasswordError: {},

  resettingPassword: false,
  resettingPasswordSuccess: {},
  resettingPasswordError: {},
};
export default auth;

const setting = {
  authPage: {
    path: "login",
    message: "Use your account details to login",
  },
  sideMenu: {},
  selectedMenu: "home",
  showDrawer: false,
};
export default setting;

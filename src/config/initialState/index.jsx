import auth from "./auth";
import setting from "./setting";

export const initialState = {
  auth,
  setting,
};

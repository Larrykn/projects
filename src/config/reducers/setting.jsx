import { createReducer } from "@reduxjs/toolkit";
import { initialState } from "../initialState";
import { settingActions } from "../actions";

const setting = createReducer(initialState.setting, {
  [settingActions.switchAuthPage]: (state, action) => {
    return {
      ...state,
      authPage: action.payload,
    };
  },
  [settingActions.selectSideTab]: (state, action) => {
    console.log(action);
    return {
      ...state,
      selectSideTab: action.payload,
    };
  },
});

export default setting;

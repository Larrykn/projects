import { createReducer } from "@reduxjs/toolkit";
import { initialState } from "../initialState";
import { authActions } from "../actions";

const auth = createReducer(initialState.auth, {
  [authActions.registerApplicantRequest]: (state, action) => {
    console.log(action);
  },
  [authActions.getAuthUserRequest]: (state, action) => {
    if (action.payload !== undefined) {
      return {
        ...state,
        isAuthenticated: true,
      };
    }
  },
  [authActions.loginUser]: (state, action) => {
    console.log(action);
  },
});

export default auth;

const pathnames = {
  dashboard: { path: "/" },
  login: {
    path: "login",
    message: "Use your account details to login",
  },
  register: {
    path: "register",
    message: "Fill in the required fields to register",
  },
  recoverPassword: {
    path: "recover-password",
    message: "Use your email address to recover your password",
  },
};

export const subPathnames = {
  home: {
    mainPath: "home",
    subPath: null,
    tailPath: null,
  },
  forms: {
    mainPath: "forms",
    subPath: "application form",
    tailPath: "personal details",
  },
  vote: {
    mainPath: "vote",
    subPath: null,
    tailPath: null,
  },
  guide: {
    mainPath: "guide",
    subPath: "page 1",
    tailPath: null,
  },
  profile: {
    mainPath: "profile",
    subPath: "account details",
    tailPath: null,
  },
};

export default pathnames;

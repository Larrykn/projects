import auth from "../../reducers/auth";
import setting from "../../reducers/setting";

const rootReducer = {
  auth,
  setting,
};
export default rootReducer;

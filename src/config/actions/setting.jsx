import { createAction } from "@reduxjs/toolkit";

const settingActions = {
  switchAuthPage: createAction("SELECT_AUTH_PAGE"),
  selectSideTab: createAction("SELECT_ACTIVE_TAB"),
  showDrawer: createAction("SHOW_DRAWER"),
};

export default settingActions;

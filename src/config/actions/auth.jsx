import { createAction } from "@reduxjs/toolkit";

const auth = {
  registerApplicantRequest: createAction("REGISTER_APPLICANT_REQUEST"),
  registerApplicantSuccess: createAction("REGISTER_APPLICANT_SUCCESS"),
  registerApplicantError: createAction("REGISTER_APPLICANT_ERROR"),

  loginUserRequest: createAction("REGISTER_APPLICANT_REQUEST"),
  loginUserSuccess: createAction("REGISTER_APPLICANT_SUCCESS"),
  loginUserError: createAction("REGISTER_APPLICANT_ERROR"),

  changePasswordRequest: createAction("CHANGE_PASSWORD_REQUEST"),
  changePasswordSuccess: createAction("CHANGE_PASSWORD_SUCCESS"),
  changePasswordError: createAction("CHANGE_PASSWORD_ERROR"),

  changeDefaultPasswordRequest: createAction("CHANGE_DEFAULT_PASSWORD_REQUEST"),
  changeDefaultPasswordSuccess: createAction("CHANGE_DEFAULT_PASSWORD_SUCCESS"),
  changeDefaultPasswordError: createAction("CHANGE_DEFAULT_PASSWORD_ERROR"),

  requestToken: createAction("REQUEST_TOKEN_REQUEST"),
  requestTokenSuccess: createAction("REQUEST_TOKEN_SUCCESS"),
  requestTokenError: createAction("REQUEST_TOKEN_ERROR"),

  getAuthUserRequest: createAction("GET_AUTH_USER_REQUEST"),
  getAuthUserSuccess: createAction("GET_AUTH_USER_SUCCESS"),
  getAuthUserError: createAction("GET_AUTH_USER_ERROR"),

  resetPasswordRequest: createAction("RESET_PASSWORD_REQUEST"),
  resetPasswordSuccess: createAction("RESET_PASSWORD_SUCCESS"),
  resetPasswordError: createAction("RESET_PASSWORD_ERROR"),

  logoutUserRequest: createAction("LOGOUT_REQUEST"),
  logoutUserSuccess: createAction("LOGOUT_SUCCESS"),
  logoutUserError: createAction("LOGOUT_ERROR"),

  verifyEmailRequest: createAction("VERIFY_EMAIL_REQUEST"),
  verifyEmailSuccess: createAction("VERIFY_EMAIL_SUCCESS"),
  verifyEmailError: createAction("VERIFY_EMAIL_ERROR"),

  setAuthUserRequest: createAction("SET_AUTH_USER"),

  setIsAuthenticated: createAction("SET_IS_AUTHENTICATED"),

  removeAuthUser: createAction("REMOVE_AUTH_USER"),

  emailVerificationLinkRequest: createAction(
    "EMAIL_VERIFICATIION_LINK_REQUEST"
  ),
  emailVerificationLinkSuccess: createAction(
    "EMAIL_VERIFICATIION_LINK_SUCCESS"
  ),
  emailVerificationLinkError: createAction("EMAIL_VERIFICATIION_LINK_ERROR"),
};
// const authE = {
//   REGISTER_APPLICANT_REQUEST,
//   REGISTER_APPLICANT_SUCCESS,
//   REGISTER_APPLICANT_ERROR,

//   LOGIN_USER_REQUEST,
//   REGISTER_USER_SUCCESS,
//   REGISTER_USER_ERROR,

//   LOGOUT_REQUEST,
//   LOGOUT_SUCCESS,
//   LOGOUT_ERROR,

//   CHANGE_PASSWORD_REQUEST,
//   CHANGE_PASSWORD_SUCCESS,
//   CHANGE_PASSWORD_ERROR,

//   CHANGE_DEFAULT_PASSWORD_REQUEST,
//   CHANGE_DEFAULT_PASSWORD_SUCCESS,
//   CHANGE_DEFAULT_PASSWORD_ERROR,

//   REQUEST_TOKEN_REQUEST,
//   REQUEST_TOKEN_SUCCESS,
//   REQUEST_TOKEN_ERROR,

//   EMAIL_VERIFICATIION_LINK_REQUEST,
//   EMAIL_VERIFICATIION_LINK_SUCCESS,
//   EMAIL_VERIFICATIION_LINK_ERROR,

//   RESET_PASSWORD_REQUEST,
//   RESET_PASSWORD_SUCCESS,
//   RESET_PASSWORD_ERROR,

//   VERIFY_EMAIL_REQUEST,
//   VERIFY_EMAIL_SUCCESS,
//   VERIFY_EMAIL_ERROR,

//   GET_AUTH_USER_SUCEESS,
//   GET_AUTH_USER_REQUEST,
//   GET_AUTH_USER_ERROR,

//   SET_AUTH_USER,
//   SET_IS_AUTHENTICATED,
//   REMOVE_AUTH_USER,
// };

export default auth;

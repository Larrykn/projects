import { useState, useEffect } from "react";

const useWindowSize = () => {
  const [dimensions, setDimensions] = useState({
    width: undefined,
    height: undefined,
  });
  const width = dimensions.width;

  useEffect(() => {
    //set initial device width
    if (!width) {
      setDimensions({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }

    const useWindowSize = () =>
      setDimensions({
        width: window.innerWidth,
        height: window.innerHeight,
      });

    window.addEventListener("resize", useWindowSize);
    return () => window.removeEventListener("resize", useWindowSize);
  }, [dimensions.width, dimensions.height]);

  return { ...dimensions };
};

export default useWindowSize;

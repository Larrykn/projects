import React, { Suspense } from "react";
import pathname from "./config/routes";
import { Routes, Route } from "react-router-dom";
import { PageLoader } from "./ui/molecules";
import { PageNotFound } from "./ui/organisms";
import "antd/dist/antd.css";
import "bootstrap/dist/css/bootstrap.min.css";

const ControlPanel = React.lazy(() => import("./ui/organisms/dashboard"));

const App = () => {
  return (
    <>
      <Suspense fallback={<PageLoader />}>
        <Routes>
          <Route path={pathname.dashboard.path} element={<ControlPanel />} />
          <Route path="*" element={<PageNotFound />} /> //for testing only,to be
          changed
        </Routes>
      </Suspense>
    </>
  );
};

export default App;
